angular.module('starter.ngGeolocation', [])

.factory('$Geolocation', ['$rootScope', '$timeout', function($rootScope, $timeout) {

  // watch id update
  var watchID = null;

  // success geolocation
  var onSuccess = function(position) {
    $timeout(function() {
      $rootScope.$broadcast('$Geolocation:onSuccessPosition', position);
    });
  }

  // error geolocation
  var onFailure = function(err) {
    $timeout(function() {
      $rootScope.$broadcast('$Geolocation:onFailurePosition', err);
    });
  }

  return {
    // current position
    currentPosition: function(options) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onSuccess, onFailure, options);
      } else {
        alert("Geolocation is not supported by this browser or device.");
      }
    },

    // watch position
    updatePosition: function(options) {
      if (navigator.geolocation) {
        watchID = navigator.geolocation.watchPosition(onSuccess, onFailure, options);
      } else {
        alert("Geolocation is not supported by this browser or device.");
      }
    },

    // clear watch position
    cleanPosition: function() {
      if (watchID != null) {
        navigator.geolocation.clearWatch(watchID);
        watchID = null;
      }
    }
  }



}])
