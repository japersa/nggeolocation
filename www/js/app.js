angular.module('starter', ['ionic', 'starter.ngGeolocation'])

.run(function($ionicPlatform, $Geolocation, $rootScope) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  $Geolocation.updatePosition({
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 10000
  });

  $rootScope.$on('$Geolocation:onSuccessPosition', function(event, position) {
    alert("Latitude: " + position.coords.latitude +
      "\nLongitude: " + position.coords.longitude);
  });

  $rootScope.$on('$Geolocation:onFailurePosition', function(event, err) {
    switch (err.code) {
      case err.PERMISSION_DENIED:
        alert("User denied the request for Geolocation.");
        break;
      case err.POSITION_UNAVAILABLE:
        alert("Location information is unavailable.");
        break;
      case err.TIMEOUT:
        alert("The request to get user location timed out.");
        break;
      case err.UNKNOWN_ERROR:
        alert("An unknown error occurred.");
        break;
    }
  });

})
